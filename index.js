const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const route = require('./app/routes');
const ErrorHandlerMiddleware = require('./app/utils/ErrorHandlerMiddleware');
const { PREFIX } = require('./app/config/AppConfig');


const app = express();
const { PORT = 5000 } = process.env;
app.use(bodyParser.json());


// middlewares
app.use(morgan('dev'));


// routes
app.use(PREFIX, route);
app.use(ErrorHandlerMiddleware.MainHandler);

// starting the server
app.listen(PORT, () => {
  console.log('Escuchando puerto:', PORT);
});

module.exports = app;
