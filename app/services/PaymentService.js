const PaymentService = module.exports;
const Paypal = require('../utils/Paypal');
const log4js = require('../utils/logger');

const defaultLogger = log4js.getLogger('UsersServices');
const PaymentsConfig = require('../config/PaymentsConfig');
const PaymentRepository = require('../Repositories/PaymentRepository');


PaymentService.create = async (pay, options) => {
  const { logger = defaultLogger } = options;
  logger.info(
    `Start PaymentServices.create: body ${JSON.stringify(pay)}`,
  );

  const {
    name, sku, price, total, description,
  } = pay;
  let href;
  try {
    const { links } = await Paypal.create(
      PaymentsConfig.create(name, sku, price, total, description),
    );

    logger.info(
      `Start PaymentServices.create: body ${JSON.stringify(links)}`,
    );
    if (links) {
      links.forEach((link) => {
        if (link.rel === 'approval_url') {
          href = link.href;
        }
      });
    }

    return { link: href };
  } catch (error) {
    return null;
  }
};

PaymentService.success = async (payment, options) => {
  const { logger = defaultLogger } = options;
  logger.info(
    `Start PaymentServices.success: body ${JSON.stringify(payment)}`,
  );
  const { PayerID, paymentId } = payment;
  try {
    const { transactions: [transaction] } = await Paypal.getPayment(paymentId);
    const { amount } = transaction;
    const response = await Paypal.execute(paymentId, PaymentsConfig.execute(PayerID, amount));

    if (response) {
      const { id, payer } = response;
      const payerId = payer.payer_info.payer_id;

      const result = await PaymentRepository.createTransaction({ paymentId: id, payerId });

      console.log(result);

      return { success: true };
    }

    return null;
  } catch (error) {
    return { error };
  }
};
